#ifndef AI_HPP
#define AI_HPP
#include "../ai/gpt2tc.h"

#include <string>
#include <functional>



class Ai {
    GPT2ModelEnum model;
    std::string model_path;

public:
    Ai(const std::string& model, const std::string& model_path);
    ~Ai();

    std::string complete(const std::string& input_text, std::function<bool (char, int)> cb = nullptr,
                         int top_k = DEFAULT_TOP_K, float top_p = DEFAULT_TOP_P, float temperature = 1.0,
                         int max_output_len = MAX_OUTPUT_LEN, int seed = 0);
};

#endif // AI_HPP
