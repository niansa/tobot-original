#ifndef AIBASEMSGS_HPP
#define AIBASEMSGS_HPP
#include <string_view>

namespace basemsgs {
extern std::string_view talk;
}

#endif // AIBASEMSGS_HPP
