#include "../ai/gpt2tc.h"
#include "ai.hpp"

#include <string>
#include <string_view>
#include <sstream>
#include <memory>
#include <functional>
#include <cstring>
#include <sys/time.h>



Ai::Ai(const std::string& model, const std::string& model_path)
    : model(parse_model(model.c_str())), model_path(model_path) {
    //
}
Ai::~Ai() {
    //
}

std::string Ai::complete(const std::string& input_text, std::function<bool (char, int)> cb,
                         int top_k, float top_p, float temperature,
                         int max_output_len, int seed) {
    std::ostringstream fres;
    TextCompleteGlobalState *tcs;
    TextGenContext *ts;
    int count;
    struct timeval tv;
    std::unique_ptr<const char>(input_text1);
    struct list_head ts_list;

    tcs = text_complete_global_init(model, model_path.c_str());

    if (seed == 0) {
        gettimeofday(&tv, NULL);
        seed = tv.tv_sec + tv.tv_usec;
    }

    input_text1.reset(trim_text(input_text.c_str()));
    if (input_text1.get()[0] == '\0') {
        input_text1.reset(strdup(" "));
    }

    ts = text_complete_start(tcs, input_text1.get(), top_k, top_p, temperature,
                             seed, max_output_len);
    count = 0;
    for(;;) {
        init_list_head(&ts_list);
        list_add_tail(&ts->link, &ts_list);
        text_complete_next(tcs, &ts_list);
        if (ts->out_text_len == 0)
            break;
        auto str = std::string_view{ts->out_text, static_cast<std::string_view::size_type>(ts->out_text_len)};
        if (cb) {
            bool brk = false;
            for (const char character : str) {
                count++;
                if (!cb(character, count)) {
                    brk = true;
                    break;
                }
                fres << character;
            }
            if (brk) {
                break;
            }
        } else {
            fres << str;
        }
    }
    text_complete_end(ts);

    text_complete_global_end(tcs);

    return fres.str();
}
